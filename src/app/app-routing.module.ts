import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// components
import { UserDetailComponent } from "./views/user-detail/user-detail.component";
import { HomeComponent } from "./views/home/home.component";
import { LoginComponent } from "./views/login/login.component";
import { SignupComponent } from "./views/signup/signup.component";
import { LogoutComponent } from "./views/logout/logout.component";

const routes: Routes = [
	{ path: "", component: HomeComponent },
	{ path: "user/:id", component: UserDetailComponent },
	{ path: "login", component: LoginComponent },
	{ path: "signup", component: SignupComponent },
	{ path: "logout", component: LogoutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
