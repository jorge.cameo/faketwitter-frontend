import { createReducer, on, createSelector, createFeatureSelector } from "@ngrx/store";
import { addError } from "./error.action";

export const initialState = {
	message: "",
	isShown: false,
};

export const errorReducer = createReducer(
	initialState, 
	on(addError, (state, payload) => ({...state, payload})),
);

export const getError = createFeatureSelector<any[]>("errors");

export const errorState = createSelector(
	getError,
	state => {
		return state;
	}
);