import { createAction, props } from "@ngrx/store";

export const addFollower = createAction(
	"[Follower Component] Follower", 
	props<any>()
);