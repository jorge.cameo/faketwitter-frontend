import { createAction, props } from "@ngrx/store";

import { IUser } from "../interfaces/iuser";

export const followUser = createAction(
	"[Following Component] Following",
	props<IUser>()
);