import { 
	createReducer, 
	createSelector, 
	createFeatureSelector, 
	on, 
} from "@ngrx/store";

import { IUser } from "../interfaces/iuser";

import { followUser } from "./following.action";

export const initialState = [{}];

export const followingReducer = createReducer(
	initialState, 
	on(followUser, (state, payload) => ([...state, payload]))
);

export const getFollowingState = createFeatureSelector<IUser[]>("following");

export const followingState = createSelector(
	getFollowingState, 
	state => {
		return state.map(item => {
			return {
				item
			}
		})
	}
);