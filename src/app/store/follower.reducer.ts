import { createReducer, on, createSelector, createFeatureSelector } from "@ngrx/store";
import { addFollower } from "./follower.action";

import { IUser } from "../interfaces/iuser";

export const initialState = [
	{}
	//{id: 1, username: "jorgeluis", photo: "profile-2.png", fullName: "Jorge Luis"},
];

export const followerReducer = createReducer(
	initialState, 
	on(addFollower, (state, payload) => ([...state, payload])),
);

export const getFollowersState = createFeatureSelector<any[]>("followers");

export const followersState = createSelector(
	getFollowersState,
	state => {
		return state.map(item => {
			return {
				item
			}
		})
	}
);