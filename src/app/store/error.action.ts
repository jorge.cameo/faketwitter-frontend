import { createAction, props } from "@ngrx/store";

export const addError = createAction(
	"[Error Component] Error", 
	props<any>()
);