import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Reducers
import { followerReducer } from "./store/follower.reducer";
import { followingReducer } from "./store/following.reducer";
import { errorReducer } from "./store/error.reducer";
// Components
import { ProfileComponent } from './components/profile/profile.component';
import { TrendsComponent } from './components/trends/trends.component';
import { FollowersComponent } from './components/followers/followers.component';
import { ProfileNumbersComponent } from './components/profile-numbers/profile-numbers.component';
import { FollowingComponent } from './components/following/following.component';
import { EffectsModule } from '@ngrx/effects';
import { TweetFormComponent } from './components/tweet-form/tweet-form.component';
import { TweetFeedComponent } from './components/tweet-feed/tweet-feed.component';

import { UserDetailComponent } from './views/user-detail/user-detail.component';
import { HomeComponent } from './views/home/home.component';
import { LoginComponent } from './views/login/login.component';
import { SignupComponent } from './views/signup/signup.component';
import { MyTweetsComponent } from './components/my-tweets/my-tweets.component';
import { FollowingToComponent } from './components/following-to/following-to.component';
import { LogoutComponent } from './views/logout/logout.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UserComponent } from './components/user/user.component';
// primeng-components
import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {ImageModule} from 'primeng/image';
import {AvatarModule} from 'primeng/avatar';
import {SpeedDialModule} from 'primeng/speeddial';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {BadgeModule} from 'primeng/badge';
import {PasswordModule} from 'primeng/password';
import {InputTextModule} from 'primeng/inputtext';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
// Effects
import { FollowingEffects } from "./effects/following.effect";

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    //
    HttpClientModule,
    StoreModule.forRoot(
      { 
        followers: followerReducer,
        following: followingReducer,
        errors: errorReducer
      },
    ),
    EffectsModule.forRoot([FollowingEffects]),
    // prime-ng
    ButtonModule,
    CardModule,
    ImageModule,
    AvatarModule,
    SpeedDialModule,
    InputTextareaModule,
    BadgeModule,
    PasswordModule,
    InputTextModule,
    MessagesModule,
    MessageModule
  ],
  declarations: [
    AppComponent,
    // components
    ProfileComponent,
    TrendsComponent,
    FollowersComponent,
    ProfileNumbersComponent,
    FollowingComponent,
    TweetFormComponent,
    TweetFeedComponent,
    UserDetailComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    MyTweetsComponent,
    FollowingToComponent,
    LogoutComponent,
    NavbarComponent,
    UserComponent,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class AppModule { }
