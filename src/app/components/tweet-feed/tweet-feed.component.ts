import { Component, OnInit } from '@angular/core';

import { Observable } from "rxjs";

import { MenuItem } from 'primeng/api';

import { MyTweetsService } from "../../services/my-tweets.service";
import { IMyTweet } from "../../interfaces/my-tweet";

@Component({
  selector: 'app-tweet-feed',
  templateUrl: './tweet-feed.component.html',
  styleUrls: ['./tweet-feed.component.scss']
})
export class TweetFeedComponent implements OnInit {

  selectedTweet: string = "";
	tweets?: IMyTweet[] = [];
  actionItems: MenuItem[] = [
    {
      icon: 'pi pi-pencil',
      command: () => {
        console.log("pencil");
      }
    },
    {
      icon: 'pi pi-trash',
      command: ($event) => {
        this.deleteTweetFromList();
      }
    },
    {
      icon: 'pi pi-upload',
      routerLink: ['/fileupload']
    },
  ];

  selectTweet(tweet: any) {
    this.selectedTweet = tweet._id;
  }

	getTweetsList(): void {
		this.tweetsService.getTweets()
			.subscribe(data => {
        this.tweets = data.tweets;
      });
	}

  deleteTweetFromList(): void {
    this.tweetsService.deleteTweet(this.selectedTweet)
      .subscribe(data => {
        window.location.href = "/";
      });
  }

  hitLike(tweet: any): void {
    let likePayload = { username: tweet.username, photo: tweet.photo };
    this.tweetsService.addTweetLike(likePayload, tweet._id)
      .subscribe(data => {
        window.location.href = "/";
      });
  }

  getLikes(tweet: any): number {
    return tweet.likes ? tweet.likes.length : 0;
  }

  constructor(private tweetsService: MyTweetsService) { }

  ngOnInit(): void {
  	this.getTweetsList();
  }

}
