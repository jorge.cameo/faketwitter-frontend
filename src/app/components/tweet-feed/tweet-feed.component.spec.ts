import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TweetFeedComponent } from './tweet-feed.component';
// services
import { MyTweetsService } from "../../services/my-tweets.service";

describe('TweetFeedComponent', () => {
  let component: TweetFeedComponent;
  let fixture: ComponentFixture<TweetFeedComponent>;

  let getTweetsList;

  beforeEach(async () => {
    let myTweetsService = jasmine.createSpyObj("MyTweetsService", ["getTweets"]);
    getTweetsList = myTweetsService.getTweets.and.returnValue(of());

    await TestBed.configureTestingModule({
      providers: [
        { provide: MyTweetsService, useValue: myTweetsService }
      ],
      declarations: [ TweetFeedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TweetFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('TweetFeed should be created', () => {
    expect(component).toBeTruthy();
  });

  it("tweets list -> Should return empty array", () => {
    expect(component.tweets).toEqual([]);
  });

  it("selectTweet -> Should return selected id", () => {
    component.selectTweet({_id: "1", message: "tweet example"});
    expect(component.selectedTweet).toEqual("1");
  });

  it("getLikes -> Should return 0", () => {
    let tweet = { _id: "1", message: "tweet 1" };
    expect(component.getLikes(tweet)).toEqual(0);
  });
});
