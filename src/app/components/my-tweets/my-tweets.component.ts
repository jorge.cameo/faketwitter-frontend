import { Component, OnInit, Input } from '@angular/core';

import { MenuItem } from 'primeng/api';

import { MyTweetsService } from "../../services/my-tweets.service";
import { IMyTweet } from "../../interfaces/my-tweet";

@Component({
  selector: 'app-my-tweets',
  templateUrl: './my-tweets.component.html',
  styleUrls: ['./my-tweets.component.scss']
})
export class MyTweetsComponent implements OnInit {
  @Input() username?: string;

  tweets: IMyTweet[] = [];
  actionItems: MenuItem[] = [
    {
      icon: 'pi pi-pencil',
      command: () => {
        console.log("pencil");
      }
    },
    {
      icon: 'pi pi-trash',
      command: () => {
        console.log("trash")
      }
    },
    {
      icon: 'pi pi-upload',
      routerLink: ['/fileupload']
    },
  ];

	getMyTweetsList(): void {
		this.tweetsService.getMyTweets()
			.subscribe(data => {
        this.tweets = data.tweets;
      });
	}

  getUserTweetsList(): void {
    let username = this.username ? this.username : "/";
    this.tweetsService.getUserTweets(username)
      .subscribe(data => {
        this.tweets = data.tweets;
      });
  }

  hitLike(tweet: any): void {
    let likePayload = { username: tweet.username, photo: tweet.photo };
    this.tweetsService.addTweetLike(likePayload, tweet._id)
      .subscribe(data => {
        window.location.href = "/";
      });
  }

  getLikes(tweet: any): number {
    return tweet.likes ? tweet.likes.length : 0;
  }

  constructor(private tweetsService: MyTweetsService) { }

  ngOnInit(): void {
    if (this.username === undefined) {
  	  this.getMyTweetsList();
    } else {
      this.getUserTweetsList();
    }
  }

}
