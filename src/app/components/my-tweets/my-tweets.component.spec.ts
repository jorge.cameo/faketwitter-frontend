import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTweetsComponent } from './my-tweets.component';
import { MyTweetsService } from "../../services/my-tweets.service";

describe('MyTweetsComponent', () => {
  let component: MyTweetsComponent;
  let fixture: ComponentFixture<MyTweetsComponent>;
  
  let getMyTweets;

  beforeEach(async () => {
    let myTweetsService = jasmine.createSpyObj(
      "MyTweetsService", 
      [ "getMyTweets" ]
    );

    getMyTweets = myTweetsService.getMyTweets.and.returnValue(of())

    await TestBed.configureTestingModule({
      providers: [
        { provide: MyTweetsService, useValue: myTweetsService },
      ],
      declarations: [ MyTweetsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyTweetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('MyTweetsComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("getMyTweetsList should -> should return empty array", () => {
    expect(component.tweets).toEqual([]);
  });
});
