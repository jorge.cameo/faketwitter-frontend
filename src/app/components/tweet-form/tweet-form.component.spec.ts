import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TweetFormComponent } from './tweet-form.component';

import { MyTweetsService } from "../../services/my-tweets.service";
import { ProfileService } from "../../services/profile.service";

describe('TweetFormComponent', () => {
  let component: TweetFormComponent;
  let fixture: ComponentFixture<TweetFormComponent>;

  let publishTweet;
  let getProfile;

  beforeEach(async () => {
    let profileService = jasmine.createSpyObj("ProfileService", ["getProfile"]);
    let myTweetsService = jasmine.createSpyObj("MyTweetsService", ["addTweet"])
    
    getProfile = profileService.getProfile.and.returnValue(of());
    publishTweet = myTweetsService.addTweet.and.returnValue(of())

    await TestBed.configureTestingModule({
      providers: [
        { provide: MyTweetsService, useValue: profileService },
        { provide: ProfileService, useValue: profileService }
      ],
      declarations: [ TweetFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TweetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('TweetForm should be created', () => {
    expect(component).toBeTruthy();
  });

  it("showAddImage -> toggleAddImage should be true", () => {
    component.showAddImage();
    expect(component.toggleAddImage).toBe(true);
  });
});
