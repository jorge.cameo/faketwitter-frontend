import { Component, OnInit } from '@angular/core';

import { ProfileService } from "../../services/profile.service";
import { MyTweetsService } from "../../services/my-tweets.service";

import { IMyTweet } from "../../interfaces/my-tweet";
import { IUser } from "../../interfaces/iuser";

@Component({
  selector: 'app-tweet-form',
  templateUrl: './tweet-form.component.html',
  styleUrls: ['./tweet-form.component.scss']
})
export class TweetFormComponent implements OnInit {

	image?: string = "";
	message?:string = "";
	toggleAddImage: boolean = false;
	tweet?: IMyTweet;
	profile?: IUser;

	publishTweet() {
		if (this.profile) {
			let tweetData = {
				id: Math.floor(Math.random()*100),
				username: this.profile.username,
				fullName: this.profile.fullName,
				image: this.image,
				message: this.message,
				photo: this.profile.photo
			};
			this.myTweetsService.addTweet(tweetData)
				.subscribe(data => {
					this.image = "";
					this.message = "";
					this.toggleAddImage = false;

					window.location.href = "/";
				});
		}
	}

	showAddImage() {
		this.toggleAddImage = !this.toggleAddImage;
	}

  constructor(
  	private profileService: ProfileService, 
  	private myTweetsService: MyTweetsService
	) { }

  ngOnInit(): void {
  	this.profileService.getProfile()
  		.subscribe(data => this.profile = data);
  }

}
