import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

// component
import { ProfileComponent } from './profile.component';
// services
import { ProfileService } from "../../services/profile.service";

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  let getProfile;

  beforeEach(async () => {
    let profileService = jasmine.createSpyObj("ProfileService", ["getProfile"]);
    getProfile = profileService.getProfile.and.returnValue(of());

    await TestBed.configureTestingModule({
      declarations: [ ProfileComponent ],
      providers: [
        { provide: ProfileService, useValue: profileService }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it("ProfileComponent should be created", () => {
    expect(component).toBeTruthy();
  });

  it("profile value should return undefined", () => {
    expect(component.profile).toBeUndefined();
  });

  it("getProfile -> Should return has no errors", () => {
    component.getProfile();
    expect(component.hasError).toBe(false);
  });
});
