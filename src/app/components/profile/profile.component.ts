import { Component, OnInit } from '@angular/core';

import { ProfileService } from "../../services/profile.service";
import { FollowingService } from "../../services/following.service";
import { IUser } from "../../interfaces/iuser";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

	profile?: IUser;
  hasError: boolean = false;

	getProfile(): void {
    this.profileService.getProfile()
      .subscribe(
        data => {
          this.profile = data.user;
          this.hasError = false;
        },
        err => {
          this.hasError = true;
        });
	}

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
  	this.getProfile();
  }

}
