import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowingComponent } from './following.component';
// services
import { UsersService } from "../../services/users.service";
import { FollowingService } from "../../services/following.service";

describe('FollowingComponent', () => {
  let component: FollowingComponent;
  let fixture: ComponentFixture<FollowingComponent>;

  let loadUsers;
  let addUserFollower;

  beforeEach(async () => {
    let usersService = jasmine.createSpyObj("UsersService", ["getUsers"])
    let followingService = jasmine.createSpyObj("FollowingService", ["addFollowingUser"]);
    
    addUserFollower = followingService.addFollowingUser.and.returnValue(of());
    loadUsers = usersService.getUsers.and.returnValue(of());

    await TestBed.configureTestingModule({
      providers: [
        { provide: UsersService, useValue: usersService },
        { provide: FollowingService, useValue: followingService },
      ],
      declarations: [ FollowingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FollowingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('FollowingComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("loadUsers -> Should return empty list", () => {
    expect(component.users).toEqual([]);
  });

  it("addUserFollower -> Should return no errors", () => {
    let user = {
      id: 1,
      fullName: "User 1",
      username: "user-1",
      photo: "pic-1.png",
    };
    component.addUserFollower(user);
    expect(component.hasError).toEqual(false);
  });
});
