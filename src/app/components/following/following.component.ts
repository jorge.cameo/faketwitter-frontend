import { Component, OnInit } from '@angular/core';

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
// interfaces
import { IUser } from "../../interfaces/iuser";
// actions
import { followUser } from "../../store/following.action";
import { addError } from "../../store/error.action";
// services
import { UsersService } from "../../services/users.service";
import { FollowingService } from "../../services/following.service";

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.scss']
})
export class FollowingComponent implements OnInit {

	users?: IUser[] = [];
  error?: string;
  hasError: boolean = false;

	addUserFollower(user: IUser) {
    this.followingService.addFollowingUser(user)
      .subscribe(
        data => {
		      //this.store.dispatch(followUser(data));
          this.hasError = false;
          window.location.href = "/";
        },
        err => {
          this.hasError = true;
          //this.store.dispatch(addError({message: err.message, isShown: true}))
        }
      );
	}

	loadUsers(): void {
    this.usersService.getUsers()
      .subscribe(data => { this.users = data.users });
  }

  constructor(
  	private usersService: UsersService, 
    private followingService: FollowingService, 
    //private store: Store<IUser>
	) { }

  ngOnInit(): void {
  	this.loadUsers();
  }

}
