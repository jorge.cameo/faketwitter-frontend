import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import { FollowingService } from "../../services/following.service";
import { MyTweetsService } from "../../services/my-tweets.service";
import { IUser } from "../../interfaces/iuser";

@Component({
  selector: 'app-profile-numbers',
  templateUrl: './profile-numbers.component.html',
  styleUrls: ['./profile-numbers.component.scss']
})
export class ProfileNumbersComponent implements OnInit {
	
	followingUsers?: IUser[];

  followingCount: string = "0";
  myTweetsCount: string = "0";

	getProfileFollowing(): void {
		this.followingUsersService.getFollowingUsers()
			.subscribe(following => {
        this.followingCount = following.followingUsers.length.toString();
      });
	}

  getFollowingSelector(): void {
    let followingState = this.store.select(state => state.following);
    followingState.subscribe(item => {
      this.followingCount = item.length.toString();
      this.followingUsers = item;
    });
  }

  getMyTweets(): void {
    this.myTweetsService.getMyTweets()
      .subscribe(result => {
        this.myTweetsCount = result.tweets.length.toString();
      });
  }

  constructor(
  	private followingUsersService: FollowingService, 
    private myTweetsService: MyTweetsService, 
    private store: Store<any>
	) { }

  ngOnInit(): void {
  	//this.getFollowingSelector();
    this.getProfileFollowing();
    this.getMyTweets();
  }

}
