import { of } from "rxjs";
import { Store } from "@ngrx/store";
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileNumbersComponent } from './profile-numbers.component';
// services
import { MyTweetsService } from "../../services/my-tweets.service";
import { FollowingService } from "../../services/following.service";

describe('ProfileNumbersComponent', () => {
  let component: ProfileNumbersComponent;
  let fixture: ComponentFixture<ProfileNumbersComponent>;

  let getProfileFollowing;
  let getMyTweets;

  beforeEach(async () => {
    let myTweetsService = jasmine.createSpyObj("MyTweetsService", ["getMyTweets"]);
    let followingService = jasmine.createSpyObj("FollowingService", ["getFollowingUsers"]);

    let store = jasmine.createSpyObj("FollowingStore", ["getFollowingState"]);

    getMyTweets = myTweetsService.getMyTweets.and.returnValue(of());
    getProfileFollowing = followingService.getFollowingUsers.and.returnValue(of());

    await TestBed.configureTestingModule({
      providers: [
        { provide: MyTweetsService, useValue: myTweetsService },
        { provide: FollowingService, useValue: followingService },
        { provide: Store, useValue: store },
      ],
      declarations: [ ProfileNumbersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProfileNumbersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('ProfileNumbersComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("myTweetsCount -> should return 0", () => {
    expect(component.myTweetsCount).toEqual("0");
  });

  it("followingCount -> should return 0", () => {
    expect(component.followingCount).toEqual("0");
  });
});
