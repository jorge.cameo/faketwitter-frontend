import { Component, OnInit } from '@angular/core';

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
// interfaces
import { IUser } from "../../interfaces/iuser";
import { addFollower } from "../../store/follower.action";
// services
import { UsersService } from "../../services/users.service";
import { FollowingService } from "../../services/following.service";

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss']
})
export class FollowersComponent implements OnInit {

	users?: IUser[] = [];
  hasError: Boolean = false;

	addUserFollower(user: IUser) {
    this.followingService.addFollowingUser(user)
      .subscribe(
        user => {
  		    //this.store.dispatch(addFollower(user));
          this.hasError = false;
        },
        err => {
          this.hasError = true;
        }
      )
	}

  
  /*loadFollowers(): void {
    //this.followers$ = this.store.select("followers");
    //this.store.select("followers").subscribe(item => this.followers$ = item);
    this.users = this.store.select("followrs").lift();
  }*/

  /*constructor(private store: Store<any>) {
  }*/

  loadUsers(): void {
    this.usersService.getUsers().subscribe(data => this.users = data);
  }

  constructor(
    private usersService: UsersService, 
    private followingService: FollowingService, 
    //private store: Store<IUser>
  ) {}

  ngOnInit(): void {
    this.loadUsers();
  }

}
