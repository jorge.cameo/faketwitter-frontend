import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowersComponent } from './followers.component';

import { UsersService } from "../../services/users.service";
import { FollowingService } from "../../services/following.service";

describe('FollowersComponent', () => {
  let component: FollowersComponent;
  let fixture: ComponentFixture<FollowersComponent>;

  let loadUsers;

  beforeEach(async () => {
    let usersService = jasmine.createSpyObj("UsersService", ["getUsers"])
    let followingService = jasmine.createSpyObj("FollowingService", ["addFollowingUser"]);

    loadUsers = usersService.getUsers.and.returnValue(of());

    await TestBed.configureTestingModule({
      providers: [
        { provide: UsersService, useValue: usersService },
        { provide: FollowingService, useValue: followingService },
      ],
      declarations: [ FollowersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FollowersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('FolloweresComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("loadUsers -> return empty array", () => {
    expect(component.users).toEqual([]);
  });
});
