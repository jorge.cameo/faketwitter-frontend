import { Component, OnInit } from '@angular/core';

// services
import {FollowingService } from "../../services/following.service";

@Component({
  selector: 'app-following-to',
  templateUrl: './following-to.component.html',
  styleUrls: ['./following-to.component.scss']
})
export class FollowingToComponent implements OnInit {

	users?: any;
  hasError: boolean = false;

	loadUsers(): void {
    this.followingService.getFollowingUsers()
      .subscribe(
        data => { 
          this.users = data.followingUsers;
          this.hasError = false;
        },
        err => {
          this.hasError = true;
        });
  }

  constructor(private followingService: FollowingService) { }

  ngOnInit(): void {
  }

}
