import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowingToComponent } from './following-to.component';
// services
import { FollowingService } from "../../services/following.service";

describe('FollowingToComponent', () => {
  let component: FollowingToComponent;
  let fixture: ComponentFixture<FollowingToComponent>;

  let loadUsers;

  beforeEach(async () => {
    let followingService = jasmine.createSpyObj("FollowingService", ["getFollowingUsers"]);
    loadUsers = followingService.getFollowingUsers.and.returnValue(of());

    await TestBed.configureTestingModule({
      providers: [
        { provide: FollowingService, useValue: followingService }
      ],
      declarations: [ FollowingToComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FollowingToComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('FollowingToComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("loadUsers -> return with no errors", () => {
    component.loadUsers();
    expect(component.hasError).toEqual(false);
  });
});
