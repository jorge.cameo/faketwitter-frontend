import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendsComponent } from './trends.component';

import { MyTweetsService } from "../../services/my-tweets.service";

describe('TrendsComponent', () => {
  let component: TrendsComponent;
  let fixture: ComponentFixture<TrendsComponent>;

  let getTweets;

  var mockTweets = [
    { _id: 1, message: "tweet #1" },
    { _id: 2, message: "tweet #2" },
    { _id: 3, message: "tweet #3" },
  ];

  beforeEach(async () => {
    let myTweetsService = jasmine.createSpyObj("MyTweetsService", ["getTweets"]);

    getTweets = myTweetsService.getTweets.and.returnValue(of());

    await TestBed.configureTestingModule({
      providers: [
        { provide: MyTweetsService, useValue: myTweetsService },
      ],
      declarations: [ TrendsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('TrendsComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("getTweets -> should return with no errors", () => {
    component.getTweets();
    expect(component.hasError).toEqual(false);
  });

  it("getTweets -> should return with empty tweets array", () => {
    component.getTweets();
    expect(component.tweets).toEqual([]);
  });
});
