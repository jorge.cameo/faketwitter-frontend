import { Component, OnInit } from '@angular/core';

import { ITrend } from "../../interfaces/itrend";
import { MyTweetsService } from "../../services/my-tweets.service";

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {
  tweets: any[] = [];
  hasError: boolean = false;

  getTweets(): void {
    this.tweetsService.getTweets()
      .subscribe(
        data => {
          this.tweets = data.tweets;
          this.hasError = false;
        },
        err => {
          this.hasError = true;
        }
      );
  }

  constructor(
    private tweetsService: MyTweetsService
  ) { }

  ngOnInit(): void {
    this.getTweets();
  }

}
