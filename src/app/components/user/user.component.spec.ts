import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user.component';
// services
import { ProfileService } from "../../services/profile.service";

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  let getUserInfo;

  var mockUserResult = {
    user: {
      username: "user1",
      fullName: "User 1",
      email: "user1@mail.com",
      photo: "user-1-pic.jpg",
      tweets: [],
      followings: [],
    },
    status: 200
  };

  beforeEach(async () => {
    let profileService = jasmine.createSpyObj("ProfileService", ["getUserDetails"]);
    getUserInfo = profileService.getUserDetails.and.returnValue(of(mockUserResult));

    await TestBed.configureTestingModule({
      providers: [
        { provide: ProfileService, useValue: profileService }
      ],
      declarations: [ UserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('UserComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("getUserInfo -> return no errors", () => {
    component.getUserInfo();
    expect(component.hasError).toEqual(false);
  })

  it("getUserInfo -> user property should return response value", () => {
    component.username = "user1";
    component.getUserInfo();
    expect(component.user).toEqual(mockUserResult.user);
  });
});
