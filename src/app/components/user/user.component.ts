import { Component, OnInit, Input } from '@angular/core';

import { ProfileService } from "../../services/profile.service";
import { FollowingService } from "../../services/following.service";
import { IUserDetail } from "../../interfaces/iuserdetail";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() username?: string;
  user?: IUserDetail;
  hasError: boolean = false;

  getUserInfo(): void {
    let username = this.username ? this.username : "";
    this.profileService.getUserDetails(username)
      .subscribe(
        data => { 
          this.user = data.user
          this.hasError = false;
        },
        err => {
          this.hasError = true;
        }
      );
  }

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.getUserInfo();
  }

}
