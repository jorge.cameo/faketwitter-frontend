import { Injectable } from "@angular/core";

import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { map, mergeMap, catchError } from "rxjs/operators";
import { FollowingService } from "../services/following.service";

@Injectable()
export class FollowingEffects {
	loadFollowingUsers = createEffect(() => this.actions$.pipe(
		ofType("[Following Page] Following"),
		mergeMap(() => this.followingService.getFollowingUsers()
			.pipe(
				map(users => ({ type: "[Following API] Following users loaded successfully", payload: users})),
				catchError(() => of({ type: "[Following API] Following users error"}))
			)
		)
	))

	constructor(
		private actions$: Actions,
		private followingService: FollowingService
	) { }
}	