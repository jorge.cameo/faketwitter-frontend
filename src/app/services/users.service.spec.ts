import { of } from "rxjs";
import { TestBed, inject, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule, HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;
  let httpClientSpy: { get: jasmine.Spy };

  let mockUnauthorizedResult = {
    error: "Token is missing",
    status: 400
  };

  let mockUsersResult = {
    users: [
      { username: "user1", fullName: "user 1", email: "user1@mail.com" },
      { username: "user2", fullName: "user 2", email: "user2@mail.com" },
      { username: "user3", fullName: "user 3", email: "user3@mail.com" },
    ],
    status: 200
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
    	imports: [
        HttpClientModule,
        HttpClientTestingModule 
      ],
    	providers: [ UsersService ]
    });
    httpClientSpy = jasmine.createSpyObj(["HttpClient", "get"]);
    service = new UsersService(httpClientSpy as any);
  });

  it(`should issue a request`, () => {
    expect(service).toBeTruthy();
  });

  it("Get all users [GET] -> Should return 400", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockUnauthorizedResult));

  	service.getUsers().subscribe(data => {
  		expect(data.status).toEqual(400);
      done();
  	});

  	/*const req = httpTestingController.expectOne(service.endpoint);
  	expect(req.request.method).toEqual("GET");
    done();*/
  });

  it("Get all users [GET] -> Should return error result", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockUnauthorizedResult));

    service.getUsers().subscribe(result => {
      expect(result).toEqual(mockUnauthorizedResult);
      done();
    })
  });

  it("Get all users [GET] -> Should return 200", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockUsersResult));

    service.getUsers().subscribe(result => {
      expect(result.status).toEqual(200);
      done();
    });
  });
});
