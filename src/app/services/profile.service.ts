import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { IUser } from "../interfaces/iuser";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
	endpoint: string = "http://localhost:3000/api/v1/user";
	headers?: any;

	getProfile(): Observable<any> {
		/*let accessToken = localStorage.getItem("access_token");
  	let headers = new HttpHeaders().set("Authorization", `Bearer ${accessToken}`);*/
		return this.http.get<IUser>(this.endpoint + "/me", { headers: this.headers });
	}

	setHeaders(): void {
		let accessToken = localStorage.getItem("access_token");
  	this.headers = new HttpHeaders().set("Authorization", `Bearer ${accessToken}`);
	}

	getUserDetails(username: string): Observable<any> {
		return this.http.get<any>(`${this.endpoint}/${username}`, {headers: this.headers});
	}

  constructor(private http: HttpClient) {
  	this.setHeaders();
  }
}
