import { of } from "rxjs";
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { AuthService } from './auth.service';
import { ILoginUser } from "../interfaces/iloginuser";
import { ISignupUser } from "../interfaces/isignupuser";

describe('AuthService', () => {
  let service: AuthService;
  let httpClientSpy: { post: jasmine.Spy };

  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;

  let mockLoginValidResult = {
    accessToken: "a-jwt-fake-token",
    user: {
      _id: "some-random-id-1",
      username: "jorgeluis",
      fullName: "Jorge Luis",
      email: "jorgel6m02@gmail.com"
    },
    status: 201
  };
  let mockLoginInvalidCredentialsResult = {
    error: "Wrong credentials",
    status: 401,
  };
  let mockLoginNotExistResult = {
    error: "User doesn't exist!",
    status: 401
  };
  let mockLoginMissingCredentialsResult = {
    error: "Missing credentials",
    status: 400
  };

  let mockSignupValidResult = (userData: any) => {
    return { user: userData, status: 201 };
  }

  let mockSignupExistentResult = {
    error: "User already exists with same username or email",
    status: 409
  };

  beforeEach(() => {
  	TestBed.configureTestingModule({ 
  		imports: [ HttpClientTestingModule ],
    	providers: [ AuthService ]
  	});

    httpClientSpy = jasmine.createSpyObj("HttpClient", ["post"]);
  	service = new AuthService(httpClientSpy as any);

  	httpClient = TestBed.inject(HttpClient);
  	httpTestingController = TestBed.inject(HttpTestingController);
  });

 	afterEach(() => {
    httpTestingController.verify();
  });

  it("auth service should be created", () => {
    expect(service).toBeTruthy();
  });

  it("login [POST] -> should return 201", (done: DoneFn) => {

    httpClientSpy.post.and.returnValue(of(mockLoginValidResult));

  	service.loginUser({ username: "jorgeluis", password: "31mayo1991" }).subscribe(result => {
  		expect(result.status).toEqual(201);
      done();
  	});

  	/*const req = httpTestingController.expectOne(service.endpoint + "/login");
  	expect(req.request.method).toEqual("POST");*/
  });

  it("login (invalid-credentials) [POST] -> should return 400", (done: DoneFn) => {

    httpClientSpy.post.and.returnValue(of(mockLoginMissingCredentialsResult));

  	let loginData: ILoginUser = { username: "jorgesa" };
  	service.loginUser(loginData).subscribe(data => {
  		expect(data.status).toEqual(400);
      done();
  	});

  	/*const req = httpTestingController.expectOne(service.endpoint + "/login");
  	expect(req.request.method).toEqual("POST");*/
  });

  it("login (invalid-credentials) [POST] -> should return result (invalid-credentials)", (done: DoneFn) => {

    httpClientSpy.post.and.returnValue(of(mockLoginMissingCredentialsResult));

    let loginData: ILoginUser = { username: "jorgesa" };
    service.loginUser(loginData).subscribe(data => {
      expect(data).toEqual(mockLoginMissingCredentialsResult);
      done();
    });

    /*const req = httpTestingController.expectOne(service.endpoint + "/login");
    expect(req.request.method).toEqual("POST");*/
  });

  it("login (unexitent user) [POST] -> should return 401", (done: DoneFn) => {

    httpClientSpy.post.and.returnValue(of(mockLoginNotExistResult));

  	service.loginUser({ username: "jorgesa", password: "pass123" }).subscribe(data => {
  		expect(data.status).toEqual(401);
      done();
  	});
  	/*const req = httpTestingController.expectOne(service.endpoint + "/login");
  	expect(req.request.method).toEqual("POST");*/
  });

  it("login (unexitent user) [POST] -> should result (unexistent user)", (done: DoneFn) => {

    httpClientSpy.post.and.returnValue(of(mockLoginNotExistResult));

    service.loginUser({ username: "jorgesa", password: "pass123" }).subscribe(data => {
      expect(data).toEqual(mockLoginNotExistResult);
      done();
    });
    /*const req = httpTestingController.expectOne(service.endpoint + "/login");
    expect(req.request.method).toEqual("POST");*/
  });

  it("signup (existent user) [POST] -> should return 409", (done: DoneFn) => {

  	let registerData: ISignupUser = {
  		username: "jorgeluis",
  		fullName: "Jorge Luis",
  		email: "jorgel6m02@gmail.com",
  		password: "pass123",
  		photo: "profile-1.png"
  	};

    httpClientSpy.post.and.returnValue(of(mockSignupExistentResult));

  	service.registerUser(registerData).subscribe(data => {
  		expect(data.status).toEqual(409);
      done();
  	});

  	/*const req = httpTestingController.expectOne(service.endpoint + "/signup");
  	expect(req.request.method).toEqual("POST");*/
  });

  it("signup (existent-user) [POST] -> should return result (existent-user)", (done: DoneFn) => {

    let registerData: ISignupUser = {
      username: "jorgeluis",
      fullName: "Jorge Luis",
      email: "jorgel6m02@gmail.com",
      password: "pass123",
      photo: "profile-1.png"
    };

    httpClientSpy.post.and.returnValue(of(mockSignupExistentResult));

    service.registerUser(registerData).subscribe(data => {
      expect(data.status).toEqual(409);
      done();
    });

    /*const req = httpTestingController.expectOne(service.endpoint + "/signup");
    expect(req.request.method).toEqual("POST");*/
  });

  it("signup (new user) [POST] -> should return 201", (done: DoneFn) => {
    let registerData: ISignupUser = {
      username: "jlcameo",
      fullName: "Jorge Luis Cameo",
      email: "jlcameo@mail.com",
      password: "pass123",
      photo: "profile-3.png"
    };

    httpClientSpy.post.and.returnValue(of(mockSignupValidResult(registerData)));

  	service.registerUser(registerData).subscribe(data => {
  		expect(data.status).toEqual(201);
      done();
  	});

  	/*const req = httpTestingController.expectOne(service.endpoint + "/signup");
  	expect(req.request.method).toEqual("POST");*/
  });
});
