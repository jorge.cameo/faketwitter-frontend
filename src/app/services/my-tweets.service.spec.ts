import { of } from "rxjs";
import { TestBed } from '@angular/core/testing';

import { MyTweetsService } from './my-tweets.service';

describe('MyTweetsService', () => {
  let service: MyTweetsService;
  let httpClientSpy: { get: jasmine.Spy };

  let mockSuccessfulResult = {
    tweets: [
      { _id: "1", message: "tweet #1" },
      { _id: "2", message: "tweet #2" },
      { _id: "3", message: "tweet #3" },
    ],
    status: 200
  };

  let mockErrorResult = {
    error: "",
    status: 400
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj(["HttpClient", "get"])
    service = new MyTweetsService(httpClientSpy as any);
  });

  it("MyTweetsService should be created", () => {
  	expect(service).toBeTruthy();
  });

  it("getTweets -> Should return 200", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockSuccessfulResult));

    service.getTweets().subscribe(result => {
      expect(result.status).toBe(200);
      done();
    });
  });

  it("getTweets -> Should return tweets list length", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockSuccessfulResult));

    service.getTweets().subscribe(result => {
      expect(result.tweets.length).toBe(3);
      done();
    });
  });
});
