import { Injectable } from '@angular/core';

import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { IUser } from "../interfaces/iuser";

@Injectable({
  providedIn: 'root'
})
export class FollowersService {

	headers?: any;

	addFollowerUser(user: IUser): Observable<IUser> {
		let loggedUsername = localStorage.getItem("")
		return this.http.post<IUser>("http://localhost:3000/user", user, { headers: this.headers });
	}

	setHeaders(): void {
		let accessToken = localStorage.getItem("access_token");
  	this.headers = new HttpHeaders().set("Authorization", `Bearer ${accessToken}`);
	}

  constructor(private http: HttpClient) {
  	this.setHeaders();
  }
}
