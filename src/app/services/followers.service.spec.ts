import { TestBed } from '@angular/core/testing';

import { FollowersService } from './followers.service';

describe('FollowersService', () => {
  let service: FollowersService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
    service = new FollowersService(httpClientSpy as any);
  });

  it("FollowerService is created", (done: DoneFn) => {
  	expect(service).toBeTruthy();
  	done();
  });
});
