import { Injectable } from '@angular/core';

import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
//
import { ILoginUser } from "../interfaces/iloginuser";
import { ISignupUser } from "../interfaces/isignupuser";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

	endpoint: string = "http://localhost:3000/api/v1/auth"

	registerUser(userData: ISignupUser): Observable<any> {
		return this.http.post<any>(this.endpoint + "/signup", userData);
	}

	loginUser(loginData: ILoginUser): Observable<any> {
		return this.http.post<any>(this.endpoint + "/login", loginData);
	}

  constructor(private http: HttpClient) { }
}
