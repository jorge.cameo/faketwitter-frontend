import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

import { IUser } from "../interfaces/iuser";

@Injectable({
  providedIn: 'root'
})
export class FollowingService {

	endpoint?: string = "http://localhost:3000/api/v1/user/following";
	headers?: any;
	following?: any;

	getFollowingUsers(): Observable<any> {
		return this.http
			.get<any>(this.endpoint + "/", { headers: this.headers })
			.pipe(map((following) => following || []));
	}

	addFollowingUser(user: any): Observable<any> {
		let userData = {...user, ...{ isFollowing: true }};
		return this.http.post<any>(
			this.endpoint + "/", 
			userData, 
			{ headers: this.headers }
		);
	}

	setHeaders(): void {
		let accessToken = localStorage.getItem("access_token");
  	this.headers = new HttpHeaders().set("Authorization", `Bearer ${accessToken}`);
	}

  constructor(private http: HttpClient) {
  	this.setHeaders();
  }
}
