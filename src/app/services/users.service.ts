import { Injectable } from '@angular/core';

import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { IUser } from "../interfaces/iuser";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

	headers?: any;
	endpoint: string = "http://localhost:3000/api/v1/user";

	getUsers(): Observable<any> {
		return this.http.get<any>(this.endpoint + "/", { headers: this.headers });
	}

	setHeaders(): void {
		let accessToken = localStorage.getItem("access_token");
  	this.headers = new HttpHeaders().set("Authorization", `Bearer ${accessToken}`);
	}

  constructor(private http: HttpClient) {
  	this.setHeaders();
  }
}
