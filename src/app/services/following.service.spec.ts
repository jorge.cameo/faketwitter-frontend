import { TestBed } from '@angular/core/testing';

import { FollowingService } from './following.service';

describe('FollowingService', () => {
  let service: FollowingService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
    service = new FollowingService(httpClientSpy as any);
  });

  it("FollowingService is created", () => {
  	expect(service).toBeTruthy();
  });
});
