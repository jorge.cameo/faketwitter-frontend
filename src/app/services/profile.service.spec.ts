import { of } from "rxjs";
import { TestBed } from '@angular/core/testing';

import { ProfileService } from './profile.service';

describe('ProfileService', () => {
  let service: ProfileService;
  let httpClientSpy: { get: jasmine.Spy };

  var mockSuccessfulResult = {
    user: {
      _id: "1",
      username: "user-1",
      fullName: "User 1",
      email: "user1@mail.com",
      photo: "pic-user-1.png"
    },
    status: 200
  };

  var mockNotFoundResult = {
    error: "Not user found!",
    status: 404
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj(["HttpClient", "get"]);
    service = new ProfileService(httpClientSpy as any);
  });

  it("ProfileService should be created", () => {
  	expect(service).toBeTruthy();
  });

  it("getProfile -> Should return 200 status", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockSuccessfulResult));

    service.getProfile().subscribe(result => {
      expect(result.status).toBe(200);
      done();
    });
  });

  it("getProfile -> Should return user object", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockSuccessfulResult));

    service.getProfile().subscribe(result => {
      expect(result).toBe(mockSuccessfulResult);
      done();
    });
  });

  it("getUserDetails (non-existent user) -> Should return 404 status", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockNotFoundResult));

    service.getUserDetails("user1x").subscribe(result => {
      expect(result.status).toBe(404);
      done();
    });
  });

  it("getUserDetails (non-existent user) -> Should return bad result", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(mockNotFoundResult));

    service.getUserDetails("user1x").subscribe(result => {
      expect(result).toBe(mockNotFoundResult);
      done();
    });
  });
});
