import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { IMyTweet } from "../interfaces/my-tweet";

@Injectable({
  providedIn: 'root'
})
export class MyTweetsService {

	headers?: any;
	endpoint?: string = "http://localhost:3000/api/v1/tweet";

	addTweet(tweet: IMyTweet): Observable<any> {
		//https://s.yimg.com/ny/api/res/1.2/RnEb2XbilzXA4ieEb5MaTg--/YXBwaWQ9aGlnaGxhbmRlcjt3PTk2MDtoPTY0MDtjZj13ZWJw/https://s.yimg.com/uu/api/res/1.2/dVScEcoP6_pFXT5NE6U1Iw--~B/aD04MDA7dz0xMjAwO2FwcGlkPXl0YWNoeW9u/https://media.zenfs.com/es/the_independent_uses_512/d86f53e229a6336cb776ab2f8a52b122
		return this.http.post<any>(this.endpoint + "/", tweet, { headers: this.headers });
	}

	addTweetLike(likePayload: any, tweetId: string): Observable<any> {
		return this.http.post<any>(this.endpoint + "/" + tweetId + "/likes", likePayload, { headers: this.headers });
	}

	getTweets(): Observable<any> {
		return this.http.get<any>(this.endpoint + "/", { headers: this.headers });
	}

	getMyTweets(): Observable<any> {
		return this.http.get<any>(this.endpoint + "/me", { headers: this.headers });
	}

	getUserTweets(username: string): Observable<any> {
		return this.http.get<any>(`${this.endpoint}/${username}`, { headers: this.headers });
	}

	// delete
	deleteTweet(tweetId: string): Observable<any> {
		return this.http.delete<any>(`${this.endpoint}/${tweetId}`, { headers: this.headers });
	}

	setHeaders(): void {
		let accessToken = localStorage.getItem("access_token");
  	this.headers = new HttpHeaders().set("Authorization", `Bearer ${accessToken}`);
	}

  constructor(private http: HttpClient) {
  	this.setHeaders();
  }
}
