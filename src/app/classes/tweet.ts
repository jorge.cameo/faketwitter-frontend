export class Tweet {
	id?: number;
	username?: string;
	fullName?: string;
	imgUrl?: string;
	message?: string;

	constructor(
		id: number, 
		username: string, 
		fullName: string,
		imgUrl: string,
		message: string
	){
		this.id = id;
		this.username = username;
		this.fullName = fullName;
		this.imgUrl = imgUrl;
		this.message = message;
	}
}
