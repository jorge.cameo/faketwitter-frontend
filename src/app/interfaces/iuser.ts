export interface IUser {
	id: number;
	fullName: string;
	username: string;
	photo: string;
}
