export interface IUserDetail {
	username: string;
	fullName: string;
	email: string;
	photo: string;
	tweets: string[];
	followings: string[];
}
