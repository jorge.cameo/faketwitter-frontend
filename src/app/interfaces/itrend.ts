export interface ITrend {
	id: number;
	title: string;
	tweetsCount: number;
}