import { IUser } from "./iuser";

export interface IProfile extends IUser {
	id: number;
	fullName: string;
	username: string;
	photo: string;
	following: number;
	tweets: number;
}