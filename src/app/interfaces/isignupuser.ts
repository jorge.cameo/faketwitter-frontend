export interface ISignupUser {
	username?: string;
	fullName?: string;
	email?: string;
	photo?: string;
	password?: string;
}
