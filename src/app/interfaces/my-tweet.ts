export interface IMyTweet {
	_id?: string;
	username?: string;
	fullName?: string;
	author?: string;
	image?: string;
	message?: string;
	photo?: string;
	likes?: any[];
}
