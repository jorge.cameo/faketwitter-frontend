import { Component, OnInit } from '@angular/core';

import { Router } from "@angular/router";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

	counter: number = 4;
	interval?: any;

  constructor(private router: Router) { }

  decrementCounter(): void {
  	if (this.counter === 0) {
  		this.counter = 4;
  		localStorage.clear();
  		this.router.navigateByUrl("/login");
  		clearInterval(this.interval);
  	} else {
  		this.counter -= 1;
  	}
  }

  ngOnInit(): void {
  	this.interval = setInterval(() => {
  		this.decrementCounter();
  	}, 1000);
  }

}
