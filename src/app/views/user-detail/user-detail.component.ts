import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";

import { ProfileService } from "../../services/profile.service";
import { IUser } from "../../interfaces/iuser";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

	userId?: string;
  profile?: IUser;
  hasError: boolean = false;

  getProfile(): void {
    this.profileService.getProfile()
      .subscribe(
        profile => { 
          this.profile = profile; 
          this.hasError = false;
        },
        err => {
          this.hasError = true;
        });
  }

  constructor(
    private profileService: ProfileService,
    route: ActivatedRoute
  ) {
  	route.params.subscribe((params) => {
  		this.userId = params["id"];
  	})
  }

  ngOnInit(): void {
    this.getProfile();
  }

}
