import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";

import { UserDetailComponent } from './user-detail.component';
// services
import { ProfileService } from "../../services/profile.service";

describe('UserDetailComponent', () => {
  let component: UserDetailComponent;
  let fixture: ComponentFixture<UserDetailComponent>;
  let router: Router;

  let getProfile;

  var mockSuccessfulResult = {
    user: {
      id: 1,
      fullName: "User 1",
      username: "user1",
      photo: "pic-1.png"
    },
    status: 200
  };

  beforeEach(async () => {
    let profileService = jasmine.createSpyObj("ProfileService", ["getProfile"]);

    getProfile = profileService.getProfile.and.returnValue(of(mockSuccessfulResult.user));

    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule.withRoutes([]) ],
      providers: [
        { provide: ProfileService, useValue: profileService }
      ],
      declarations: [ UserDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('UserDetailComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("getProfile -> Should return with no errors", () => {
    component.getProfile();
    expect(component.hasError).toEqual(false);
  });

  it("getProfile -> Should return user info", () => {
    component.getProfile();
    expect(component.profile).toEqual(mockSuccessfulResult.user);
  });
});
