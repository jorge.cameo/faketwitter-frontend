import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
// services
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	username?: string;
	password?: string;

	hasError?: boolean = false;
	error?: any;

	authenticate() {
		this.authService.loginUser({username: this.username, password: this.password})
			.subscribe(
				res => {
					localStorage.setItem("access_token", res.accessToken);
					this.router.navigateByUrl("/");
				},
				err => {
					this.hasError = true;
					this.error = err.error;
				}
			)
	}

  constructor(
  	private authService: AuthService,
  	private router: Router
	) { }

  ngOnInit(): void {
  }

}
