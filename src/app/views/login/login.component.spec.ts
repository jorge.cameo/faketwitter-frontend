import { of } from "rxjs";
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { Router } from "@angular/router";

import { AuthService } from "../../services/auth.service";
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;

  let loginUser;

  beforeEach(async () => {
    let authService = jasmine.createSpyObj("AuthService", ["loginUser"]);
    let expectedResult = {
      accessToken: "some-token",
      user: {
        username: "jorgeluis",
        fullName: "Jorge Luis",
        email: "jorgel6m02@gmail.com"
      },
      status: 201
    }
    loginUser = authService.loginUser.and.returnValue(of(expectedResult))

    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      declarations: [ LoginComponent ],
      providers: [
        { provide: AuthService, useValue: authService}
      ]
    })
    .compileComponents();

    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('LoginComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("loginUser should -> return has no errors", () => {
    component.username = "jorgeluis";
    component.password = "31mayo1991";
    component.authenticate();

    expect(component.hasError).toBe(false);
  });
});
