import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
//
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

	fullName?: string;
	username?: string;
	email?: string;
	photo?: string;
	password?: string;

	hasError?: boolean = false;
	error?: any;

	register() {
		let signupData = {
			username: this.username, 
			email: this.email, 
			fullName: this.fullName, 
			password: this.password,
			photo: this.photo
		};
		this.authService.registerUser(signupData)
			.subscribe(
				res => {
					this.router.navigateByUrl("/login");
				},
				err => {
					this.hasError = true;
					this.error = err.error;
				}
			);
	}

  constructor(
  	private authService: AuthService,
  	private router: Router
	) { }

  ngOnInit(): void {
  }

}
