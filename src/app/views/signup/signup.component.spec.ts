import { of } from "rxjs";

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";

import { SignupComponent } from './signup.component';
import { AuthService } from "../../services/auth.service";

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let router: Router;
  let registerUser;

  let mockSignupSuccessful = {
    user: {
      email: "jorgel6m02@gmail.com",
      fullName: "Jorge Luis",
      username: "jorgeluis",
    },
    status: 201
  };

  beforeEach(async () => {

    let authService = jasmine.createSpyObj("AuthService", ["registerUser"]);

    registerUser = authService.registerUser.and.returnValue(of())

    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule.withRoutes([]) ],
      declarations: [ SignupComponent ],
      providers: [ 
        { provide: AuthService, useValue: authService }
      ]
    })
    .compileComponents();

    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SignupComponent should be created', () => {
    expect(component).toBeTruthy();
  });

  it("registerUser should return 201", () => {
    component.username = "jorgeluis";
    component.fullName = "Jorge Luis";
    component.email = "jorgel6m02@gmail.com";
    component.password = "31mayo1991";

    component.register();

    expect(component.hasError).toBe(false);
  })
});

/*describe("SignupComponent", () => {
  let component: SignupComponent;
  let authService: AuthService;
  let router: Router;
  let httpClientSpy: { post: jasmine.Spy };

  var mockSuccessfullResult = (payload: any) => {
    let [ username, fullName, email, password ] = payload;
    return {
      user: { username, password, fullName, email },
      status: 201
    };
  }

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["post"]);
    authService = new AuthService(httpClientSpy as any);
    component = new SignupComponent(authService, router);
  });

  it("SignupComponent should be created", () => {
    expect(component).toBeTruthy();
  });

  it("Register a user -> Should return 201", (done: DoneFn) => {
    httpClientSpy.post.and.returnValue(of(mockSuccessfullResult));

    component.username = "jorgeluis";
    component.email = "jorgel6m02@gmail.com";
    component.fullName = "Jorge Luis";
    component.register().subscribe(result => {

    })
  });
})
*/
